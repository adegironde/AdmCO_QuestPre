def add(x, y):
#Cette fonction prend 2 variables en arguments, les additionent et retourne un message pour afficher le nom du module ou #l'operation a lieu, puis retroune le resultat de l'addition
  z=x+y

  print('add() executed under the scope: ', __name__)

  return z

if __name__ == '__main__': #on verifie qu'on a pas importé un module
#on recupere deux variables
  x=input('Enter the first number to add: ')

  y=input('Enter the second number to add: ')
#on appelle la fonction pour additioner
  result = add(int(x),int(y))
# on realise un affichage
  print(x, '+', y,'=', result)

  print('Code executed under the scope: ', __name__)